using Scellecs.Morpeh.Systems;
using UnityEngine;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh.Elysium;
using Scellecs.Morpeh;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(VisualizeSphereSystem))]
public sealed class VisualizeSphereSystem : UpdateSystem, IUpdateSystem
{
    private Filter _entities;

    public override void OnAwake()
    {
        _entities = World.Filter.With<VisibleComponent>().With<UnityView>().Build();
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in _entities)
        {
            ref var visible = ref entity.GetComponent<VisibleComponent>();
            ref var view = ref entity.GetComponent<UnityView>();

            for (int i = 0; i < visible.ObjectsToVisualise.Length; i++)
            {
                visible.ObjectsToVisualise[i].SetActive(visible.Distance < visible.MaxShowDistance);
            }
        }
    }
}