using Game;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Elysium;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
public sealed class InputSystem : IUpdateSystem
{
    private GameSettings _gameSettings;
    private KeyboardSettings _keyboardSettings;
    private Filter _entities;

    public World World { get; set; }

    public InputSystem(GameSettings gameSettings)
    {
        _gameSettings = gameSettings;
        _keyboardSettings = _gameSettings.KeyboardSettings;
    }

    public void OnAwake()
    {
        World = World.Default;
        _entities = World.Filter.With<InputComponent>().With<MovingComponent>().Build();
    }

    public void OnUpdate(float deltaTime)
    {
        for (int i = 0; i < _keyboardSettings.InputDatas.Count; i++)
        {
            var data = _keyboardSettings.InputDatas[i];

            var hor = Input.GetAxisRaw(data.HorizontalAxis);
            var vert = Input.GetAxisRaw(data.VerticalAxis);

            foreach (var entity in _entities)
            {
                ref var input = ref entity.GetComponent<InputComponent>();
                ref var moving = ref entity.GetComponent<MovingComponent>();
                
                if(moving.Id == data.PlayerId)
                {
                    input.Horizontal = hor;
                    input.Vertical = vert;
                }
            }
        }
    }

    public void Dispose()
    {
        _entities = null;
        _keyboardSettings = null;
        _gameSettings = null;
    }
}