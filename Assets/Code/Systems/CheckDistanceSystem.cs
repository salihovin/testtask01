using Game;
using Scellecs.Morpeh;
using Scellecs.Morpeh.Elysium;
using Scellecs.Morpeh.Systems;
using System.Collections.Generic;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(CheckDistanceSystem))]
public sealed class CheckDistanceSystem : UpdateSystem, IUpdateSystem
{
    private Filter _entities;
    private LobbyViewController _controller;

    List<Vector3> _positions = new List<Vector3>();

    public CheckDistanceSystem(LobbyViewController controller)
    {
        _controller = controller;
    }

    public override void OnAwake()
    {
        _entities = World.Filter.With<MovingComponent>().With<UnityView>().Build();
    }

    public override void OnUpdate(float deltaTime)
    {
        _positions.Clear();

        foreach (var entity in _entities)
        {
            ref UnityView view = ref entity.GetComponent<UnityView>();

            _positions.Add(view.Transform.position);
        }

        if (_positions.Count == 2)
        {
            var distance = (_positions[0] - _positions[1]).magnitude;
            _controller.SetDistance(distance);

            foreach (var entity in World.Filter.With<VisibleComponent>().Build())
            {
                ref var visible = ref entity.GetComponent<VisibleComponent>();
                visible.Distance = distance;
            }

            if(distance < 1f)
            {
                var entityLoadBextlevel = World.CreateEntity();
                ref var loadTag = ref entityLoadBextlevel.AddComponent<LoadNextLevelTag>();
                loadTag.Index = 1;

                RemoveMovingEnitites();
            }

            _positions.Clear();
        }
    }

    private void RemoveMovingEnitites()
    {
        foreach (var entity in _entities)
        {
            World.RemoveEntity(entity);
        }
    }
}