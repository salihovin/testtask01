using Scellecs.Morpeh;
using Scellecs.Morpeh.Elysium;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(PauseGameSystem))]
public sealed class PauseGameSystem : UpdateSystem, IUpdateSystem
{
    private bool _paused = false;

    public override void OnAwake() { }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in World.Filter.With<PauseEvent>().Build())
        {
            entity.RemoveComponent<PauseEvent>();
            SetPause();
        }
    }

    private void SetPause()
    {
        _paused = !_paused;

        foreach (var entity in World.Filter.With<GameComponent>().Build())
        {
            if (_paused)
            {
                if (!entity.Has<PauseTag>())
                {
                    entity.AddComponent<PauseTag>();
                }
            }
            else
            {
                if (entity.Has<PauseTag>())
                {
                    entity.RemoveComponent<PauseTag>();
                }
            }
        }
    }
}