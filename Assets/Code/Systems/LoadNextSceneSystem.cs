using Scellecs.Morpeh.Systems;
using UnityEngine;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh.Elysium;
using Game;
using Scellecs.Morpeh;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(LoadNextSceneSystem))]
public sealed class LoadNextSceneSystem : UpdateSystem, IUpdateSystem
{
    private LevelManager _levelManager;
    private Filter _entities;

    private bool _loadingStarted = false;

    public LoadNextSceneSystem(LevelManager levelManager)
    {
        _levelManager = levelManager;
    }

    public override void OnAwake()
    {
        _entities = World.Filter.With<LoadNextLevelTag>().Build();
    }

    public override void OnUpdate(float deltaTime)
    {
        if(_loadingStarted) return;
        
        foreach (var entity in _entities)
        {
            _loadingStarted = true;
            
            ref var loadTag = ref entity.GetComponent<LoadNextLevelTag>();
            int index = loadTag.Index;

            World.RemoveEntity(entity);
           
            LoadLevel(index);
        }
    }

    private void LoadLevel(int index)
    {
        _levelManager.OnLoaded(() => _loadingStarted = false);
        _levelManager.UnloadCurrentAndLoad(index);
    }
}