using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh;
using UnityEngine;
using Scellecs.Morpeh.Elysium;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(MovingSystem))]
public sealed class MovingSystem : UpdateSystem, IUpdateSystem
{
    private Filter _entitiesFilter;

    public override void OnAwake()
    {
        _entitiesFilter = World.Filter.With<UnityView>()
                                      .With<MovingComponent>()
                                      .With<InputComponent>()
                                      .Build();
    }

    public override void OnUpdate(float deltaTime)
    {
        foreach (var entity in _entitiesFilter)
        {
            ref var view = ref entity.GetComponent<UnityView>();
            ref var moving = ref entity.GetComponent<MovingComponent>();
            ref var input = ref entity.GetComponent<InputComponent>();

            var direction = new Vector3(input.Horizontal, 0f, input.Vertical);

            view.Transform.position += direction * moving.Speed * deltaTime;
        }
    }
}