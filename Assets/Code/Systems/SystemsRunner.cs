using Scellecs.Morpeh;
using Scellecs.Morpeh.Elysium;
using System.Collections.Generic;
using Unity.IL2CPP.CompilerServices;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
public sealed class SystemsRunner : IUpdateSystem
{
    private Filter _entities;
    private List<ISystem> _systems;

    public World World { get; set; }

    public void OnAwake()
    {
        _entities = World.Filter.With<GameComponent>().Build();

        _systems = new List<ISystem>
        {
            new MovingSystem(),
        };

        World = World.Default;

        InitSystems();
    }

    public void Dispose()
    {
        _systems.ForEach(x => x.Dispose());
        _systems.Clear();
    }

    public void OnUpdate(float deltaTime) 
    {
        var enitiy = _entities.First();

        if(enitiy.Has<PauseTag>())
        {
            return;
        }
        
        for (int i = 0; i < _systems.Count; i++)
        {
            _systems[i].OnUpdate(deltaTime);
        }
    }

    private void InitSystems()
    {
        for (int i = 0; i < _systems.Count; i++)
        {
            _systems[i].World = World;
            _systems[i].OnAwake();
        }
    }
}