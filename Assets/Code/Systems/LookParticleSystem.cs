using Scellecs.Morpeh.Systems;
using UnityEngine;
using Unity.IL2CPP.CompilerServices;
using Scellecs.Morpeh.Elysium;
using Scellecs.Morpeh;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(LookParticleSystem))]
public sealed class LookParticleSystem : UpdateSystem, IUpdateSystem 
{
    public override void OnAwake() 
    {

    }

    public override void OnUpdate(float deltaTime) 
    {
        foreach (var entity in World.Filter.With<LookParticleComponent>().Build())
        {
            ref var look = ref entity.GetComponent<LookParticleComponent>();
            look.ObjectToLook.LookAt(look.TargetLook);
        }
    }
}