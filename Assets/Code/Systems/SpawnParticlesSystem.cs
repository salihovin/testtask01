using Scellecs.Morpeh;
using Scellecs.Morpeh.Elysium;
using Scellecs.Morpeh.Systems;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;

[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
[CreateAssetMenu(menuName = "ECS/Systems/" + nameof(SpawnParticlesSystem))]
public sealed class SpawnParticlesSystem : UpdateSystem, IUpdateSystem 
{
    private Filter _entities;

    public override void OnAwake() 
    {
        _entities = World.Filter.With<SpawnParticlesComponent>().With<StartSpawnTag>().Build();
    }

    public override void OnUpdate(float deltaTime) 
    {
        foreach (var entity in _entities)
        {
            ref var spawner = ref entity.GetComponent<SpawnParticlesComponent>(); 

            if(spawner.SpawnCount == 0)
            {
                World.RemoveEntity(entity);
            }

            if(spawner.Timer > 0)
            {
                spawner.Timer -= deltaTime;
                return;
            }

            SpawnSeveral(spawner.Pack, spawner.Prefab, spawner.BoundsCollider);
            spawner.Timer = spawner.TimePerSpawn;
            spawner.SpawnCount -= spawner.Pack;
        }
    }

    private void SpawnSeveral(int count, ParticleSystem prefab, SphereCollider boundsCollider)
    {
        for (int i = 0; i < count; i++)
        {
            Spawn(prefab, boundsCollider);
        }
    }

    private void Spawn(ParticleSystem prefab, SphereCollider boundsCollider)
    {
        var point = Random.insideUnitSphere * boundsCollider.radius;

        var particle = Instantiate(prefab); 
        particle.transform.position = boundsCollider.transform.position + point;
        particle.transform.rotation = Random.rotation;
        particle.gameObject.SetActive(true);
    }
}