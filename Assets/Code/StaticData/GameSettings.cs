using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "GameSettings", menuName = "Static Data/Game Settings" )]
    public class GameSettings : ScriptableObject
    {
        public KeyboardSettings KeyboardSettings;
    }
}
