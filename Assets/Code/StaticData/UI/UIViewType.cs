namespace Game
{
    public enum UIViewType
    {
        DebriefingView = 0,
        FlyingCoinsView = 1,
        FlyingLabelsView = 2,
        FpsMonitorView = 3,
        GameOverView = 4,
        HealthBarView = 5,
        InGameView = 6,
        LeadboardView = 7,
        LobbyView = 8,
        PopupNotifyView = 9,
        RateUsView = 10,
        SettingsView = 11,
        TouchStickView = 12,
        PreloaderView = 13,
        TransitionView = 14,
        
        ZenGameView = 15,
    }
}
