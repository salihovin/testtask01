using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game
{
    [CreateAssetMenu(menuName = "Static Data/UI Static data", fileName = "UIStaticData")]
    public class UIStaticData : ScriptableObject
    {
        public List<UIViewData> Views;
    }

    [System.Serializable]
    public class UIViewData
    {
        public UIViewType UIViewType;
        public AssetReference AssetReference;
    }
}
