using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "KeyboardSettingsSettings", menuName = "Static Data/KeyboardSettings Settings")]
    public class KeyboardSettings : ScriptableObject
    {
        public List<InputData> InputDatas;
    }

    [System.Serializable]
    public class InputData
    {
        public int PlayerId;
        public string HorizontalAxis;
        public string VerticalAxis;
    }
}
