using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
#if VCONTAINER
using VContainer;
using VContainer.Unity;
#endif
using static Scellecs.Morpeh.Elysium.EcsStartup;

namespace Scellecs.Morpeh.Elysium
{
    public sealed class EcsStartup : IDisposable
    {
        private int _currentOrder;

        private readonly Dictionary<int, SystemsGroup> _systemsGroups;
#if VCONTAINER
        private readonly LifetimeScope _scope;

        private LifetimeScope _featuresScope;
        private LifetimeScope _systemsScope;

        private Action<IContainerBuilder> _registerFeatures;
        private Action<IContainerBuilder> _registerSystems;
#endif
        private Action _buildSetupInOrder;
        private Action _setupSystemsGroups;

        private World _world;

        private bool _initialized;
        private bool _disposed;

#if VCONTAINER
        public EcsStartup(LifetimeScope scope)
        {
            this._scope = scope;
            _currentOrder = 0;
            _systemsGroups = new Dictionary<int, SystemsGroup>();
            _world = World.Default;
            _initialized = false;
            _disposed = false;
        }
#else
        public EcsStartup() 
        { 
            currentOrder = 0;
            systemsGroups = new Dictionary<int, SystemsGroup>();
            world = World.Default;
            initialized = false;
            disposed = false;
        }
#endif
        public void Initialize(bool updateByUnity)
        {
            if (_initialized)
            {
                if (_disposed)
                {
                    Debug.LogError("The EcsStartup has already been disposed. Create a new one to use it.");
                }
                else
                {
                    Debug.LogWarning($"EcsStartup with {_world.GetFriendlyName()} has already been initialized.");
                }

                return;
            }

            _world ??= World.Create();
            _world.UpdateByUnity = updateByUnity;

            RegisterFeatures();
            BuildSystemsSetupOrder();
            RegisterSystems();
            SetupSystemsGroups();
            CleanupActions();
            _initialized = true;
        }

        public StartupBuilder AddSystemsGroup() => new StartupBuilder(this, _currentOrder++);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Update(float deltaTime)
        {
            if (_world.UpdateByUnity == false)
            {
                _world.Update(deltaTime);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void FixedUpdate(float fixedDeltaTime)
        {
            if (_world.UpdateByUnity == false)
            {
                _world.FixedUpdate(fixedDeltaTime);
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void LateUpdate(float deltaTime)
        {
            if (_world.UpdateByUnity == false)
            {
                _world.LateUpdate(deltaTime);
                _world.CleanupUpdate(deltaTime);
            }
        }

        public void Dispose()
        {
            if (_initialized && _disposed == false)
            {
                _systemsGroups.Clear();
                _world.Dispose();
#if VCONTAINER
                _featuresScope.Dispose();
                _systemsScope.Dispose();
#endif
                _world = null;
                _disposed = true;
            }
        }
#if VCONTAINER
        private void AddSystemInjectedDefferedSetup<T>(int order) where T : class, ISystem
        {
            _buildSetupInOrder += () => AddSystemInjected<T>(order);
        }

        private void AddSystemInjected<T>(int order) where T : class, ISystem
        {
            _registerSystems += (builder) => builder.Register<T>(Lifetime.Transient);

            _setupSystemsGroups += () =>
            {
                var system = _systemsScope.Container.Resolve<T>();
                var systemsGroup = GetOrCreateSystemsGroup(order);
                systemsGroup.AddSystem(system);
            };
        }

        private void AddInitializerInjectedDefferedSetup<T>(int order) where T : class, IInitializer
        {
            _buildSetupInOrder += () => AddInitializerInjected<T>(order);
        }

        private void AddInitializerInjected<T>(int order) where T : class, IInitializer
        {
            _registerSystems += (builder) => builder.Register<T>(Lifetime.Transient);

            _setupSystemsGroups += () =>
            {
                var intitializer = _systemsScope.Container.Resolve<T>();
                var systemsGroup = GetOrCreateSystemsGroup(order);
                systemsGroup.AddInitializer(intitializer);
            };
        }

        private void AddFeatureInjectedDefferedSetup<T>(int order) where T : class, IEcsFeature
        {
            _registerFeatures += (builder) => builder.Register<T>(Lifetime.Transient);

            _buildSetupInOrder += () =>
            {
                var feature = _featuresScope.Container.Resolve<T>();
                feature.Configure(new FeatureBuilder(this, order));
            };
        }
#endif
        private void AddSystemDefferedSetup<T>(int order, T system) where T : class, ISystem
        {
            _buildSetupInOrder += () => AddSystem(order, system);
        }

        private void AddSystem<T>(int order, T system) where T : class, ISystem
        {
            _setupSystemsGroups += () =>
            {
                var systemsGroup = GetOrCreateSystemsGroup(order);
                systemsGroup.AddSystem(system);
            };
        }

        private void AddInitializerDefferedSetup<T>(int order, T initializer) where T : class, IInitializer
        {
            _buildSetupInOrder += () => AddInitializer(order, initializer);
        }

        private void AddInitializer<T>(int order, T initializer) where T : class, IInitializer
        {
            _setupSystemsGroups += () =>
            {
                var systemsGroup = GetOrCreateSystemsGroup(order);
                systemsGroup.AddInitializer(initializer);
            };
        }

        private void AddFeatureDefferedSetup<T>(int order, T feature) where T : class, IEcsFeature
        {
            _buildSetupInOrder += () =>
            {
                feature.Configure(new FeatureBuilder(this, order));
            };
        }

        private SystemsGroup GetOrCreateSystemsGroup(int order)
        {
            if (_systemsGroups.TryGetValue(order, out SystemsGroup systemsGroup) == false)
            {
                systemsGroup = _systemsGroups[order] = _world.CreateSystemsGroup();
            }

            return systemsGroup;
        }

        [System.Diagnostics.Conditional("VCONTAINER")]
        private void RegisterFeatures()
        {
#if VCONTAINER
            _featuresScope = _scope.CreateChild(builder => _registerFeatures?.Invoke(builder));
#endif
        }

        private void BuildSystemsSetupOrder()
        {
            _buildSetupInOrder?.Invoke();
        }

        [System.Diagnostics.Conditional("VCONTAINER")]
        private void RegisterSystems()
        {
#if VCONTAINER
            _systemsScope = _scope.CreateChild(builder => _registerSystems?.Invoke(builder));
#endif
        }

        private void SetupSystemsGroups()
        {
            _setupSystemsGroups?.Invoke();

            foreach (var group in _systemsGroups)
            {
                _world.AddSystemsGroup(group.Key, group.Value);
            }
        }

        private void CleanupActions()
        {
#if VCONTAINER
            _registerSystems = null;
            _registerFeatures = null;
#endif
            _buildSetupInOrder = null;
            _setupSystemsGroups = null;
        }

        public readonly struct StartupBuilder
        {
            private readonly EcsStartup _ecsStartup;
            private readonly int _order;

            public StartupBuilder(EcsStartup ecsStartup, int order)
            {
                this._ecsStartup = ecsStartup;
                this._order = order;
            }
#if VCONTAINER
            public StartupBuilder AddInitializerInjected<T>() where T : class, IInitializer
            {
                _ecsStartup.AddInitializerInjectedDefferedSetup<T>(_order);
                return this;
            }

            public StartupBuilder AddUpdateSystemInjected<T>() where T : class, IUpdateSystem
            {
                _ecsStartup.AddSystemInjectedDefferedSetup<T>(_order);
                return this;
            }

            public StartupBuilder AddFixedSystemInjected<T>() where T : class, IFixedSystem
            {
                _ecsStartup.AddSystemInjectedDefferedSetup<T>(_order);
                return this;
            }

            public StartupBuilder AddLateSystemInjected<T>() where T : class, ILateSystem
            {
                _ecsStartup.AddSystemInjectedDefferedSetup<T>(_order);
                return this;
            }

            public StartupBuilder AddCleanupSystemInjected<T>() where T : class, ICleanupSystem
            {
                _ecsStartup.AddSystemInjectedDefferedSetup<T>(_order);
                return this;
            }

            public StartupBuilder AddFeatureInjected<T>() where T : class, IEcsFeature
            {
                _ecsStartup.AddFeatureInjectedDefferedSetup<T>(_order);
                return this;
            }
#endif
            public StartupBuilder AddInitializer<T>(T initializer) where T : class, IInitializer
            {
                _ecsStartup.AddInitializerDefferedSetup(_order, initializer);
                return this;
            }

            public StartupBuilder AddUpdateSystem<T>(T system) where T : class, IUpdateSystem
            {
                _ecsStartup.AddSystemDefferedSetup(_order, system);
                return this;
            }

            public StartupBuilder AddFixedSystem<T>(T system) where T : class, IFixedSystem
            {
                _ecsStartup.AddSystemDefferedSetup(_order, system);
                return this;
            }

            public StartupBuilder AddLateSystem<T>(T system) where T : class, ILateSystem
            {
                _ecsStartup.AddSystemDefferedSetup(_order, system);
                return this;
            }

            public StartupBuilder AddCleanupSystem<T>(T system) where T : class, ICleanupSystem
            {
                _ecsStartup.AddSystemDefferedSetup(_order, system);
                return this;
            }

            public StartupBuilder AddFeature<T>(T feature) where T : class, IEcsFeature
            {
                _ecsStartup.AddFeatureDefferedSetup(_order, feature);
                return this;
            }
        }

        public readonly struct FeatureBuilder
        {
            private readonly EcsStartup _ecsStartup;
            private readonly int _order;

            public FeatureBuilder(EcsStartup ecsStartup, int order)
            {
                this._ecsStartup = ecsStartup;
                this._order = order;
            }
#if VCONTAINER
            public FeatureBuilder AddInitializerInjected<T>() where T : class, IInitializer
            {
                _ecsStartup.AddInitializerInjected<T>(_order);
                return this;
            }

            public FeatureBuilder AddUpdateSystemInjected<T>() where T : class, IUpdateSystem
            {
                _ecsStartup.AddSystemInjected<T>(_order);
                return this;
            }

            public FeatureBuilder AddFixedSystemInjected<T>() where T : class, IFixedSystem
            {
                _ecsStartup.AddSystemInjected<T>(_order);
                return this;
            }

            public FeatureBuilder AddLateSystemInjected<T>() where T : class, ILateSystem
            {
                _ecsStartup.AddSystemInjected<T>(_order);
                return this;
            }

            public FeatureBuilder AddCleanupSystemInjected<T>() where T : class, ICleanupSystem
            {
                _ecsStartup.AddSystemInjected<T>(_order);
                return this;
            }
#endif
            public FeatureBuilder AddInitializer<T>(T initializer) where T : class, IInitializer
            {
                _ecsStartup.AddInitializer(_order, initializer);
                return this;
            }

            public FeatureBuilder AddUpdateSystem<T>(T system) where T : class, IUpdateSystem
            {
                _ecsStartup.AddSystem(_order, system);
                return this;
            }

            public FeatureBuilder AddFixedSystem<T>(T system) where T : class, IFixedSystem
            {
                _ecsStartup.AddSystem(_order, system);
                return this;
            }

            public FeatureBuilder AddLateSystem<T>(T system) where T : class, ILateSystem
            {
                _ecsStartup.AddSystem(_order, system);
                return this;
            }

            public FeatureBuilder AddCleanupSystem<T>(T system) where T : class, ICleanupSystem
            {
                _ecsStartup.AddSystem(_order, system);
                return this;
            }
        }
    }

    public interface IEcsFeature
    {
        public void Configure(FeatureBuilder builder);
    }

    public interface IUpdateSystem : ISystem { }
}
