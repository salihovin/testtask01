using Game;
using Scellecs.Morpeh;
using UnityEngine;
using VContainer;
using VContainer.Unity;

public class GameLifetimeScope : LifetimeScope
{
    [Header("Scene refs")]
    public UIRootCanvas RootCanvas;
    public UIMediator UIMediator;
    public MonoBehaviour CoroutineRunner;

    [Header("Settings assets")]
    public LevelsPreset LevelsPreset;
    public GameSettings GameSettings;

    protected override void Configure(IContainerBuilder builder)
    {
        builder.RegisterInstance(GameSettings);
        
        builder.Register<AssetProvider>(Lifetime.Singleton);
        builder.Register<StaticDataService>(Lifetime.Singleton);
        builder.Register<SaveLoadService>(Lifetime.Singleton).As<ISaveLoadService>();

        builder.RegisterInstance(RootCanvas);
        builder.Register<UIFactory>(Lifetime.Singleton);

        builder.RegisterInstance(LevelsPreset);
        builder.Register<LevelLoader>(Lifetime.Singleton).WithParameter(CoroutineRunner);
        builder.Register<LevelManager>(Lifetime.Singleton);

        builder.RegisterInstance(World.Default);
        builder.Register<LobbyViewController>(Lifetime.Singleton);
        builder.RegisterEntryPoint<EcsModule>(Lifetime.Singleton);
    }
}
