﻿using TMPro;
using UnityEngine.UI;

namespace Game
{
    public class LobbyView : UICanvasView
    {
        public TextMeshProUGUI DistanceText;

        public void SetDistance(float distance)
        {
            DistanceText.SetText(distance.ToString());
        }
    }
}