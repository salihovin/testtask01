using Scellecs.Morpeh;
using System;
using Unity.IL2CPP.CompilerServices;

namespace Game
{
    [System.Serializable]
    [Il2CppSetOption(Option.NullChecks, false)]
    [Il2CppSetOption(Option.ArrayBoundsChecks, false)]
    [Il2CppSetOption(Option.DivideByZeroChecks, false)]
    public struct UIComponent : IComponent
    {
        public Action OnUIReady;
        public LobbyViewController LobbyViewController;
        public GameViewController GameViewController;
    } 
}