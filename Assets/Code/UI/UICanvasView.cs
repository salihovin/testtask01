﻿using UnityEngine;

namespace Game
{
    public class UICanvasView : MonoBehaviour
    {
        public int SiblingIndex = 0;
        
        public virtual void OnStart()
        {
            var rectTransform = GetComponent<RectTransform>();
            transform.localScale = Vector3.one;
            rectTransform.sizeDelta = Vector3.zero;
            rectTransform.anchoredPosition = Vector3.zero;
        }

        public virtual void Show() { }

        public virtual void Show(float duarion) { }

        public virtual void Hide() { }
    }
}