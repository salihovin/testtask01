﻿using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public class UIFactory
    {
        private AssetProvider _assetProvider;
        private StaticDataService _staticDataService;
        private UIRootCanvas _uIRootCanvas;

        public UIFactory(AssetProvider assetProvider, StaticDataService staticDataService, UIRootCanvas uIRootCanvas)
        {
            _assetProvider = assetProvider;
            _staticDataService = staticDataService;
            _uIRootCanvas = uIRootCanvas;
        }

        #region NGUI
        //public async Task<T> GetViewByTypeAsync<T>(UIViewType uIViewType, bool active = true) where T : MonoBehaviour
        //{
        //    var asset = await GetViewAssetByTypeAsync(uIViewType);
        //    var gameObject = Object.Instantiate(asset, UIRoot.transform);
        //    var view = gameObject.GetComponent<T>();

        //    if (view.TryGetComponent<UIElement>(out var uielement))
        //    {
        //        uielement.SetAnchor(UIRoot.gameObject);
        //        uielement.Active = active;
        //    }

        //    return view;
        //}

        //public T GetViewByType<T>(UIViewType uIViewType) where T : MonoBehaviour
        //{
        //    var asset = GetViewAssetByType(uIViewType);
        //    var gameObject = Object.Instantiate(asset, UIRoot.transform);
        //    var view = gameObject.GetComponent<T>();

        //    if (view == null)
        //    {
        //        view = gameObject.GetComponentInChildren<T>();
        //    }

        //    if (view.TryGetComponent<UIElement>(out var uielement))
        //    {
        //        uielement.Active = true;
        //    }

        //    return view;
        //} 
        #endregion

        #region Unity Canvas
        public async Task<T> GetCanvasViewByTypeAsync<T>(UIViewType uIViewType) where T : UICanvasView
        {
            var asset = await GetViewAssetByTypeAsync(uIViewType);
            var gameObject = Object.Instantiate(asset, _uIRootCanvas.transform);

            var view = gameObject.GetComponent<T>();

            if (view == null)
            {
                view = gameObject.GetComponentInChildren<T>(true);
            }

            view.OnStart();
            gameObject.transform.SetSiblingIndex(view.SiblingIndex);

            return view;
        }

        public T GetCanvasViewByType<T>(UIViewType uIViewType) where T : UICanvasView
        {
            var asset = GetViewAssetByType(uIViewType);
            var gameObject = Object.Instantiate(asset, _uIRootCanvas.transform);
            var view = gameObject.GetComponent<T>();

            if (view == null)
            {
                view = gameObject.GetComponentInChildren<T>();
            }

            view.OnStart();
            gameObject.transform.SetSiblingIndex(view.SiblingIndex);

            return view;
        } 
        #endregion

        private async Task<GameObject> GetViewAssetByTypeAsync(UIViewType viewType)
        {
            var viewAssetReferecne = _staticDataService.GetUIViewAsset(viewType);

            if (viewAssetReferecne != null)
            {
                var asset = await _assetProvider.LoadAsync<GameObject>(viewAssetReferecne);
                return asset;
            }
            else
            {
                Debug.Log($"View is type {viewType} not found");
                return null;
            }
        }

        private GameObject GetViewAssetByType(UIViewType viewType)
        {
            var viewAssetReferecne = _staticDataService.GetUIViewAsset(viewType);

            if (viewAssetReferecne != null)
            {
                var asset = _assetProvider.Load<GameObject>(viewAssetReferecne);
                return asset;
            }
            else
            {
                Debug.Log($"View is type {viewType} not found");
                return null;
            }
        }
    }
}
