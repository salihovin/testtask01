﻿using UnityEngine;

namespace Game
{
    public class UIMediator : MonoBehaviour
    {
        public LobbyViewController Lobby;
        public GameViewController Game;
    }
}
