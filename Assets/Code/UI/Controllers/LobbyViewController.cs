﻿using Scellecs.Morpeh;
using System;
using System.Threading.Tasks;

namespace Game
{
    [System.Serializable]
    public class LobbyViewController : IController<LobbyViewController>
    {
        public event Action OnPauseButtonPress;

        private World _eCSWorld;

        private UIFactory _factory;
        private LobbyView _lobbyView;

        public LobbyViewController(UIFactory factory, World eCSWorld)
        {
            _factory = factory;
            _eCSWorld = eCSWorld;
        }

        public void Show()
        {
            _lobbyView?.Show();
        }

        public void Hide()
        {
            _lobbyView?.Hide();
        }

        public async Task<LobbyViewController> Load()
        {
            _lobbyView = await _factory.GetCanvasViewByTypeAsync<LobbyView>(UIViewType.LobbyView);
            _lobbyView.Hide();

            return this;
        }

        public void SetDistance(float distance)
        {
            _lobbyView.SetDistance(distance);
        }

        private void SetPause()
        {
            _eCSWorld.CreateEntity().AddComponent<PauseEvent>();
        }
    }
}
