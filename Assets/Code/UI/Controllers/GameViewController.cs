﻿using System.Threading.Tasks;
using UnityEngine;

namespace Game
{
    public class GameViewController : IController<GameViewController>
    {
        private UIFactory _factory;

        public GameViewController(UIFactory factory)
        {
            _factory = factory;
        }

        public void Show()
        {
            
        }

        public void Hide()
        {
            
        }

        public async Task<GameViewController> Load()
        {
            await Task.Run(() => { });

            return this;
        }
    }
}
