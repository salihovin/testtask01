﻿using System.Threading.Tasks;

namespace Game
{
    public interface IController<T>
    {
        public void Show();
        public void Hide();
        public Task<T> Load();
    }
}
