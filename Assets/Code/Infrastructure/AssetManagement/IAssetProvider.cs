using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game
{
    public interface IAssetProvider 
    {
        Task<GameObject> Instantiate(string path, Vector3 at);
        Task<GameObject> Instantiate(string path);
        Task<GameObject> Instantiate(string path, Transform parent);
        Task<T> LoadAsync<T>(AssetReference prefabReference) where T : class;
        void Cleanup();
        Task<T> LoadAsync<T>(string address) where T : class;
        void Initialize();
    }
}