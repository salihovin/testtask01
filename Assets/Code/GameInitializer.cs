using Scellecs.Morpeh;
using VContainer;

namespace Game
{
    public sealed class GameInitializer : IInitializer
    {
        private LobbyViewController _lobbyViewController;
        private World _world;

        private LevelManager _levelManager;
        private ISaveLoadService _saveLoadService;

        public GameInitializer(LevelManager levelManager, ISaveLoadService saveLoadService)
        {
            _levelManager = levelManager;
            _saveLoadService = saveLoadService;
        }

        public World World { get => _world; set => _world = value; }



        [Inject]
        public async void LoadUI(LobbyViewController lobbyViewController)
        {
            _lobbyViewController = lobbyViewController;

            await _lobbyViewController.Load();
        }

        public void OnAwake()
        {
            World.CreateEntity().AddComponent<GameComponent>();

            var progress = _saveLoadService.LoadProgress();

            _levelManager.OnLoaded(HideCurtain);
            _levelManager.LoadLevel(progress.LevelIndex);
        }

        public void Dispose()
        {

        }


        private void HideCurtain()
        {

        }

    }
}