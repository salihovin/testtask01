using Game;
using Scellecs.Morpeh.Elysium;
using System;
using VContainer;
using VContainer.Unity;

public class EcsModule : IStartable, IDisposable
{
    private readonly LifetimeScope _scope;
    private EcsStartup _startup;

    [Inject]
    public EcsModule(LifetimeScope scope)
    {
        _scope = scope;
    }

    public void Start()
    {
        _startup = new EcsStartup(_scope);

        _startup
            .AddSystemsGroup()
            .AddInitializerInjected<GameInitializer>()
            .AddUpdateSystemInjected<InputSystem>()
            .AddUpdateSystemInjected<PauseGameSystem>()
            .AddUpdateSystemInjected<SystemsRunner>()
            .AddUpdateSystemInjected<CheckDistanceSystem>()
            .AddUpdateSystemInjected<LoadNextSceneSystem>()
            .AddUpdateSystemInjected<SpawnParticlesSystem>()
            .AddUpdateSystem(new VisualizeSphereSystem())
            .AddUpdateSystem(new LookParticleSystem())
            ;


        _startup.Initialize(updateByUnity: true);
    }

    public void Dispose()
    {
        _startup?.Dispose();
    }
}