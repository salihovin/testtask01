using Scellecs.Morpeh;
using System;
using UnityEngine;

[Serializable]
public struct SpawnParticlesComponent : IComponent
{
    public ParticleSystem Prefab;
    public SphereCollider BoundsCollider;
    public int SpawnCount;
    public int Pack;

    public float TimePerSpawn;
    public float Timer;
}
