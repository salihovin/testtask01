using Scellecs.Morpeh;
using Unity.IL2CPP.CompilerServices;
using UnityEngine;
using UnityEngine.Jobs;

[System.Serializable]
[Il2CppSetOption(Option.NullChecks, false)]
[Il2CppSetOption(Option.ArrayBoundsChecks, false)]
[Il2CppSetOption(Option.DivideByZeroChecks, false)]
public struct UnityView : IComponent 
{
    public GameObject GameObject;
    public Transform Transform;
}

public struct UnityViewNative : IComponent
{
    public TransformAccess Transform;
}