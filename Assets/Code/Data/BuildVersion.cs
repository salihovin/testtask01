using UnityEngine;

namespace DatsGames
{
	/// <summary>
	/// Данный ScriptableObject содержит информацию о номере билда, которая отображается
	/// в окне настроек игры.
	/// 
	/// Информация о номере билда записывается в данный SO в скрипте BuildVersionProcessor
	/// на этапе сборки билда, так как за рамками редактора юнити, информацию о билде
	/// невозможно получить на этапе выполнения игры на устройствах.
	/// </summary>
	
	[CreateAssetMenu(fileName = "BuildVersion", menuName = "Data/Build Version", order = 5)]
	public class BuildVersion : ScriptableObject
	{
		public string value = "0";
	}
}