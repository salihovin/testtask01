using System;

namespace Game.Data
{
    [Serializable]
    public class Vector3Data : ICloneable
    {
        public float X;
        public float Y;
        public float Z;

        public Vector3Data(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public object Clone()
        {
            return new Vector3Data(X, Y, Z);
        }
    }
}