using System;
using UnityEngine;

namespace Game
{
	[CreateAssetMenu(fileName = "LevelsPreset", menuName = "Level/Levels Preset", order = 0)]
	public class LevelsPreset : ScriptableObject
	{
		public LevelItem[] levels = new LevelItem[0];

		[Serializable]
		public class LevelItem
		{
			public string sceneName;
			public bool isAllowForRandomSelection;
		}
	}
}