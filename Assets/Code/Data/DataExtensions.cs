using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Data
{
    public static class DataExtensions
    {
        public static Vector3Data AsVectorData(this Vector3 vector) =>
          new Vector3Data(vector.x, vector.y, vector.z);

        public static Vector3 AsUnityVector(this Vector3Data vector3Data) =>
          new Vector3(vector3Data.X, vector3Data.Y, vector3Data.Z);

        public static QuaternionData AsQuaternionData(this Quaternion quaternion) =>
            new QuaternionData(quaternion.x, quaternion.y, quaternion.z, quaternion.w);

        public static Quaternion AsUnityQuaternion(this QuaternionData quaternionData) =>
            new Quaternion(quaternionData.X, quaternionData.Y, quaternionData.Z, quaternionData.W);

        public static Vector3 AddY(this Vector3 vector, float y)
        {
            vector.y = y;
            return vector;
        }

        public static float SqrMagnitudeTo(this Vector3 from, Vector3 to) => Vector3.SqrMagnitude(to - from);

        public static string ToJson(this object obj) => JsonUtility.ToJson(obj);

        public static T ToDeserialized<T>(this string json) => JsonUtility.FromJson<T>(json);

        public static bool IsLayer(this Collider collider, LayerMask layerMask) => IsLayer(collider.gameObject, layerMask);

        public static bool IsLayer(this GameObject gameObject, LayerMask layerMask) =>
            ((1 << gameObject.layer) & layerMask) != 0;

        public static void AddData<T>(this T[] Datas, T data) where T : class
        {
            var l = Datas.Length;
            Array.Resize(ref Datas, l + 1);
            Datas[l] = data;
        }

        public static T GetRandom<T>(this List<T> values) => values[Random.Range(0, values.Count)];

        public static T GetRandom<T>(this T[] values) => values[Random.Range(0, values.Length)];

        public static void LookToTarget(this Transform owner, Transform target)
        {
            var dir = target.position - owner.position;
            var rot = Quaternion.LookRotation(dir).eulerAngles;
            owner.transform.rotation = Quaternion.Euler(owner.eulerAngles.x, rot.y, owner.eulerAngles.z);
        }

        public static void LookToTarget(this Transform owner, Vector3 pos)
        {
            var dir = pos - owner.position;
            var rot = Quaternion.LookRotation(dir).eulerAngles;
            owner.transform.rotation = Quaternion.Euler(owner.eulerAngles.x, rot.y, owner.eulerAngles.z);
        }

        public static void LookToTargetBlendable(this Transform owner, Vector3 pos)
        {
            var dir = pos - owner.position;
            var rot = Quaternion.LookRotation(dir).eulerAngles;
            var newRot = Quaternion.Euler(owner.eulerAngles.x, rot.y, owner.eulerAngles.z);
            owner.transform.rotation = Quaternion.Slerp(owner.transform.rotation, newRot, Time.deltaTime * 5);
        }
    }
}