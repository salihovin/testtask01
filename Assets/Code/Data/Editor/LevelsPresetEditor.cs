using System.IO;
using UnityEngine;
using UnityEditor;
using Anthill.Utils;

namespace Game
{
    [CustomEditor(typeof(LevelsPreset))]
    public class LevelPresetEditor : Editor
    {
        #region Private Variables

        private LevelsPreset _self;
        private string[] _availScenes;

        #endregion

        #region Unity Calls

        private void OnEnable()
        {
            _self = (LevelsPreset)target;
        }

        public override void OnInspectorGUI()
        {
            // base.OnInspectorGUI();

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.Label("Levels", EditorStyles.boldLabel);
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("+ Add Level", EditorStyles.toolbarButton))
            {
                AntArray.Add(ref _self.levels, new LevelsPreset.LevelItem
                {
                    sceneName = "<Unnamed>",
                    isAllowForRandomSelection = false
                });
            }
            EditorGUILayout.EndHorizontal();

            var c = GUI.color;
            int delIndex = -1;
            int swapIndexA = -1;
            int swapIndexB = -1;
            bool hasMissedScene = false;
            for (int i = 0, n = _self.levels.Length; i < n; i++)
            {
                var item = _self.levels[i];
                EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);

                GUILayout.Toggle(false, i.ToString(), EditorStyles.toolbarButton, GUILayout.MaxWidth(30.0f));

                GUI.color = (item.isAllowForRandomSelection) ? Color.green : Color.white;
                item.isAllowForRandomSelection = GUILayout.Toggle(item.isAllowForRandomSelection, "RND", EditorStyles.toolbarButton, GUILayout.Width(40));
                GUI.color = c;

                if (!IsSceneAvail(item.sceneName))
                {
                    GUI.color = new Color(255.0f / 255.0f, 167.0f / 255.0f, 121.0f / 255.0f);
                    hasMissedScene = true;
                }

                item.sceneName = EditorGUILayout.TextField(item.sceneName, EditorStyles.toolbarTextField);
                GUI.color = c;

                GUI.enabled = (i > 0);
                if (GUILayout.Button("▲", EditorStyles.toolbarButton, GUILayout.MaxWidth(20.0f)))
                {
                    swapIndexA = i;
                    swapIndexB = i - 1;
                }

                GUI.enabled = (i + 1 < n);
                if (GUILayout.Button("▼", EditorStyles.toolbarButton, GUILayout.MaxWidth(20.0f)))
                {
                    swapIndexA = i;
                    swapIndexB = i + 1;
                }
                GUI.enabled = true;

                GUILayout.FlexibleSpace();

                GUI.color = new Color(255.0f / 255.0f, 167.0f / 255.0f, 121.0f / 255.0f);
                if (GUILayout.Button("×", EditorStyles.toolbarButton, GUILayout.MaxWidth(16.0f)))
                {
                    delIndex = i;
                }
                GUI.color = c;

                EditorGUILayout.EndHorizontal();
                _self.levels[i] = item;
            }

            if (swapIndexA > -1 && swapIndexB > -1)
            {
                AntArray.Swap(ref _self.levels, swapIndexA, swapIndexB);
            }

            if (delIndex > -1)
            {
                AntArray.RemoveAt(ref _self.levels, delIndex);
            }

            // Drag drop area.
            // ———————————————
            if (!Application.isPlaying)
            {
                DrawDragDropSceneArea();
            }

            if (hasMissedScene)
            {
                GUILayout.Space(10.0f);
                EditorGUILayout.HelpBox("This preset has one or more missing scenes! Please check that all scenes included into build settings!", MessageType.Warning);
            }

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(target);
            }
        }

        #endregion
        #region Private Methods

        private void DrawDragDropSceneArea()
        {
            Event e = Event.current;
            Rect dropRect = GUILayoutUtility.GetRect(0, 44, GUILayout.ExpandWidth(true));
            dropRect.x += 3;
            dropRect.y += 3;
            dropRect.width -= 6;
            EditorGUI.HelpBox(dropRect, "Drop Scenes here to add them into list.", MessageType.Info);

            switch (e.type)
            {
                case EventType.DragUpdated:
                case EventType.DragPerform:
                    if (!dropRect.Contains(e.mousePosition))
                    {
                        return;
                    }

                    bool isValid = true;

                    // Verify if drop is valid (contains only scenes)
                    foreach (Object dragged in DragAndDrop.objectReferences)
                    {
                        if (!dragged.ToString().EndsWith(".SceneAsset)"))
                        {
                            // Invalid
                            DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
                            isValid = false;
                            break;
                        }
                    }

                    if (!isValid)
                    {
                        return;
                    }

                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                    if (e.type == EventType.DragPerform)
                    {
                        // Add scenes
                        DragAndDrop.AcceptDrag();
                        foreach (Object dragged in DragAndDrop.objectReferences)
                        {
                            AntArray.Add(ref _self.levels, new LevelsPreset.LevelItem
                            {
                                sceneName = Path.GetFileNameWithoutExtension(AssetDatabase.GetAssetPath(dragged)),
                                isAllowForRandomSelection = false
                            });
                        }
                    }
                    break;
            }
        }

        private bool IsSceneAvail(string aSceneName)
        {
            _availScenes ??= GetAvailScenes();
            int index = System.Array.FindIndex(_availScenes, x => x.Equals(aSceneName));
            return index >= 0 && index < _availScenes.Length;
        }

        private string[] GetAvailScenes()
        {
            var result = new string[EditorBuildSettings.scenes.Length];
            for (int i = 0, n = EditorBuildSettings.scenes.Length; i < n; i++)
            {
                result[i] = Path.GetFileNameWithoutExtension(EditorBuildSettings.scenes[i].path);
            }
            return result;
        }

        #endregion
    }
}