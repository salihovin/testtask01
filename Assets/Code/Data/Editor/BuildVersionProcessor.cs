using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace DatsGames
{
	public class BuildVersionProcessor : IPreprocessBuildWithReport
	{
		public int callbackOrder { get => 0; }

		public void OnPreprocessBuild(BuildReport report)
		{
			A.Log($"BuildVersionProcessor.OnPreprocessBuild for target {report.summary.platform} at path {report.summary.outputPath}");
			var so = Resources.Load<BuildVersion>("DatsGames/BuildVersion");
#if UNITY_IOS
			so.value = PlayerSettings.iOS.buildNumber;
#elif UNITY_ANDROID
			so.value = PlayerSettings.Android.bundleVersionCode.ToString();
#endif
			EditorUtility.SetDirty(so);
			AssetDatabase.SaveAssets();
		}
	}
}