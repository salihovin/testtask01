using Game.Data;
using UnityEngine;

namespace Game
{
    public class SaveLoadService : ISaveLoadService
    {
        public string ProgressKey = "Progress";

        private PlayerProgress _playerProgress;
        
        public PlayerProgress PlayerProgress => _playerProgress;

        public void SaveProgress()
        {
            PlayerPrefs.SetString(ProgressKey, _playerProgress.ToJson());
        }

        public PlayerProgress LoadProgress()
        {
            _playerProgress = PlayerPrefs.GetString(ProgressKey)?.ToDeserialized<PlayerProgress>();

            if (_playerProgress == null)
            {
                _playerProgress = new PlayerProgress();
            }

            return _playerProgress;
        }
    }
}