namespace Game
{
    public interface ISaveLoadService
    {
        PlayerProgress PlayerProgress { get; }
        void SaveProgress();
        PlayerProgress LoadProgress();
    }
}