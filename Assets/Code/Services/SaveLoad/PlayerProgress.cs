using System;

namespace Game
{
    [Serializable]
    public class PlayerProgress
    {
        public int LevelIndex = 0;
        public int LevelNumber = 1;

        public PlayerProgress()
        {

        }
    }

}
