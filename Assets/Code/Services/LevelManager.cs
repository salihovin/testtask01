using Anthill.Utils;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace Game
{
    public class LevelManager
    {
        public struct RandomLevelItem
        {
            public int levelIndex;
            public float probability;
        }

        public delegate void LevelManagerDelegate();

        /// <summary>
        /// Событие возникающее когда процесс загрузки уровня завершен.
        /// </summary>
        public event LevelManagerDelegate EventFinishLoading;


        /// <summary>
        /// Устанавливает загруженную сцену активной (применяет настройки скайбоксов
        /// и освещения из загруженной сцены).
        /// </summary>
        public bool SetLoadedSceneAsActive = true;

        #region Private Variables

        private RandomLevelItem[] _randomLevels;
        private LevelLoader _loader;
        private LevelsPreset _levelsPreset;

        private LevelManagerDelegate _loadedCallback;
        private LevelManagerDelegate _unloadedCallback;

        #endregion

        #region Getters / Setters

        /// <summary>
        /// Определяет последнюю загруженную сцену.
        /// </summary>
        public string LastScene => (_loader.scenesHistory.Count > 0)
                ? _loader.scenesHistory[^1]
                : string.Empty;

        /// <summary>
        /// Список последних загруженных сцен.
        /// </summary>
        public List<string> ScenesHistory => _loader.scenesHistory;

        /// <summary>
        /// Определяет загружен ли какой-нибудь уровень.
        /// </summary>
        public bool IsLevelLoaded => _loader != null && _loader.HasScene;

        /// <summary>
        /// Определяет общее количество уровней в текущем наборе.
        /// </summary>
        /// <value></value>
        public int TotalLevels
        {
            get
            {
                A.Assert(_levelsPreset == null, "LevelsPreset is not setted!", "[LevelManager]");
                return _levelsPreset.levels.Length;
            }
        }

        #endregion

        #region Ctor

        public LevelManager(LevelLoader loader, LevelsPreset levelsPreset)
        {
            _loader = loader;
            _levelsPreset = levelsPreset;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Выбирает и загружает случайный уровень из LevelPreset из тех уровней
        /// которые имеют флаг RND.
        /// </summary>
        public LevelManager LoadRandom()
        {
            _randomLevels ??= InitializeRandomLevels();
            return LoadLevel(SelectRandomLevel());
        }

        /// <summary>
        /// Загружает уровень по индексу.
        /// </summary>
        /// <param name="aLevelIndex">Индекс уровня который нужно загрузить.</param>
        public LevelManager LoadLevel(int aLevelIndex)
        {
            A.Assert(_levelsPreset == null, "LevelsPreset is not setted!", "[LevelManager]");
            if (aLevelIndex >= 0 && aLevelIndex < _levelsPreset.levels.Length)
            {
                LoadLevel(_levelsPreset.levels[aLevelIndex].sceneName);
            }
            else
            {
                A.Warning($"Index of level out of bounds ({aLevelIndex}).", "[LevelManager]");
            }

            return this;
        }

        /// <summary>
        /// Загружает уровень по имени сцены.
        /// </summary>
        /// <param name="aLevelName">Имя сцены которую нужно загрузить.</param>
        public LevelManager LoadLevel(string aLevelName)
        {
            _loader.LoadScene(aLevelName)
                   .OnFinishLoading(() =>
                   {
                       if (SetLoadedSceneAsActive)
                       {
                           SceneManager.SetActiveScene(SceneManager.GetSceneByName(aLevelName));
                       }
                   
                       EventFinishLoading?.Invoke();
                       _loadedCallback?.Invoke();
                       _loadedCallback = null;
                   });
            return this;
        }

        /// <summary>
        /// Выгружает текущий уровень, выбирает и загружает случайный уровень 
        /// из LevelPreset из тех уровней которые имеют флаг RND.
        /// </summary>
        public LevelManager UnloadCurrentAndLoadRandom()
        {
            _randomLevels ??= InitializeRandomLevels();
            return UnloadCurrentAndLoad(SelectRandomLevel());
        }

        /// <summary>
        /// Выгружает уровень по имени сцены.
        /// </summary>
        /// <param name="aLevelName">Имя сцены которую нужно выгрузить.</param>
        public LevelManager UnloadLevel(string aLevelName)
        {
            _loader.UnloadScene(aLevelName)
                .OnBeginUnloading(() =>
                {
                    _unloadedCallback?.Invoke();
                    _unloadedCallback = null;
                });
            return this;
        }

        /// <summary>
        /// Выгружает текущий уровень и загружает следующий по индексу.
        /// </summary>
        /// <param name="aLevelIndex">Индекс уровня который нужно загрузить.</param>
        public LevelManager UnloadCurrentAndLoad(int aLevelIndex)
        {
            A.Assert(_levelsPreset == null, "LevelsPreset is not setted!", "[LevelManager]");
            if (aLevelIndex >= 0 && aLevelIndex < _levelsPreset.levels.Length)
            {
                UnloadCurrentAndLoad(_levelsPreset.levels[aLevelIndex].sceneName);
            }
            else
            {
                A.Warning($"Index of level out of bounds ({aLevelIndex}).", "[LevelManager]");
            }

            return this;
        }

        /// <summary>
        /// Выгружает текущий уровень и загружает следующий по имени сцены.
        /// </summary>
        /// <param name="aLevelName">Имя сцены которую нужно загрузить.</param>
        public LevelManager UnloadCurrentAndLoad(string aLevelName)
        {
            _loader.UnloadCurrentAndLoad(aLevelName)
                   .OnBeginUnloading(() =>
                   {
                       _unloadedCallback?.Invoke();
                       _unloadedCallback = null;
                   })
                   .OnFinishLoading(() =>
                   {
                       if (SetLoadedSceneAsActive)
                       {
                           SceneManager.SetActiveScene(SceneManager.GetSceneByName(aLevelName));
                       } 
                   
                       EventFinishLoading?.Invoke();
                       _loadedCallback?.Invoke();
                       _loadedCallback = null;
                   });
            return this;
        }

        /// <summary>
        /// Устанавливает одноразовый обратный вызов на событие завершения загрузки.
        /// </summary>
        /// <param name="aCallback">Обратный вызов.</param>
        public LevelManager OnLoaded(LevelManagerDelegate aCallback)
        {
            _loadedCallback = aCallback;
            return this;
        }

        /// <summary>
        /// Устанавливает одноразовый обратный вызов на событие завершения выгрузки.
        /// </summary>
        /// <param name="aCallback">Обратный вызов.</param>
        public LevelManager OnUnloaded(LevelManagerDelegate aCallback)
        {
            _unloadedCallback = aCallback;
            return this;
        }

        #endregion

        #region Private Methods

        private RandomLevelItem[] InitializeRandomLevels()
        {
            A.Assert(_levelsPreset == null, "LevelsPreset is not setted!", "[LevelManager]");

            int count = 0;
            for (int i = 0, n = _levelsPreset.levels.Length; i < n; i++)
            {
                if (_levelsPreset.levels[i].isAllowForRandomSelection)
                {
                    count++;
                }
            }

            int j = 0;
            var result = new RandomLevelItem[count];
            for (int i = 0, n = _levelsPreset.levels.Length; i < n; i++)
            {
                if (_levelsPreset.levels[i].isAllowForRandomSelection)
                {
                    result[j] = new RandomLevelItem
                    {
                        levelIndex = i,
                        probability = 1.0f
                    };
                    j++;
                }
            }

            return result;
        }

        private int SelectRandomLevel()
        {
            if (_randomLevels.Length == 0)
            {
                // Если уровни для случайного выбора отсуствуют.
                return 0;
            }

            // 1. Складываем веротяность выпадения всех уровней.
            // -------------------------------------------------
            float length = 0.0f;
            for (int i = 0, n = _randomLevels.Length; i < n; i++)
            {
                length += _randomLevels[i].probability;
            }

            // 2. Выбираем случайный уровень.
            // ------------------------------
            float rnd = AntRandom.Range(0.0f, length);
            float cur = 0.0f;
            int index = -1;
            for (int i = 0, n = _randomLevels.Length; i < n; i++)
            {
                if (rnd >= cur && rnd <= cur + _randomLevels[i].probability)
                {
                    index = i;
                    break;
                }
                cur += _randomLevels[i].probability;
            }

            // 3. Увеличиваем вероятность появления для всех уровней.
            // ------------------------------------------------------
            RandomLevelItem item;
            for (int i = 0, n = _randomLevels.Length; i < n; i++)
            {
                item = _randomLevels[i];
                item.probability += 0.05f;
                _randomLevels[i] = item;
            }

            // 4. Обнуляем шансы выпадения для выбранного уровня.
            // --------------------------------------------------
            item = _randomLevels[index];
            item.probability = 0.0f;
            _randomLevels[index] = item;

            return _randomLevels[index].levelIndex;
        }

        #endregion

        #region Event Handlers

        // ..

        #endregion
    }
}