namespace Game
{
    public class AllServices
    {
        private static AllServices _instance;
        public static AllServices Container => _instance ?? (_instance = new AllServices());

        public void RegisterSingle<TService>(TService implementation) =>
          Implementation<TService>.ServiceInstance = implementation;

        public TService Single<TService>() => Implementation<TService>.ServiceInstance;

        private class Implementation<TService>
        {
            public static TService ServiceInstance;
        }
    }
}