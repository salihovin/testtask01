using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game
{
    public class StaticDataService
    {
        private const string UIStaticDataPath = "UI/UIStaticData";


        private Dictionary<UIViewType, AssetReference> _aUIViews;

        public StaticDataService()
        {
            Load();
        }

        private StaticDataService Load()
        {
            _aUIViews = Resources.Load<UIStaticData>(UIStaticDataPath).Views
                                 .ToDictionary(x => x.UIViewType, x => x.AssetReference);

            return this;
        }

        public AssetReference GetUIViewAsset(UIViewType type) =>
               _aUIViews.TryGetValue(type, out AssetReference asset) ? asset : null;
    }
}
